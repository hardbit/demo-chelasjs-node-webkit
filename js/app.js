/**
* Modulo que llama a los servicios de clima de yahoo
*
*/

"use strict";

(function () {
	namespace("App", init, showDevTools);
	var path = require('path');

	function init()
	{
		App.ClientServices.getCustomers(function (data) {
			var wheatherData = JSON.parse(data).query.results.channel;

			$('#title').html(wheatherData.title);
			$('#condicion').text(wheatherData.item.condition.text);
			$('#humedad').text(wheatherData.atmosphere.humidity);
			$('#visibilidad').text(wheatherData.atmosphere.visibility || "--mi");
			$('#temperatura').text(wheatherData.item.condition.temp);
		});
	}

	function showDevTools() {
		require('nw.gui').Window.get().showDevTools();
	}
}) ();