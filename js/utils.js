/**
* Modulo que sobreescribe el prototipo de cadenas que agrega
* funciones tipo C#
*
*/

"use strict";

//string format equals to c#
String.prototype.format = function () {

    var pattern = /\{\d+\}/g;
    var args = arguments;
    return this.replace(pattern, function (capture) { return args[capture.match(/\d+/)]; });

}

//string formatArray equals to c#
String.prototype.formatArray = function () {
    var pattern = /\{\d+\}/g;
    var args = arguments;
    return this.replace(pattern, function (capture) { return args[0][capture.match(/\d+/)]; });

}

//- get bool from string true/false word
String.prototype.ToBoolean = function () {
    return (/^true$/i).test(this);
};


String.prototype.trim = function () {
    /// <summary>Elimina los espacios vacíos del String al inicio y al final del mismo</summary>
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}

String.prototype.trimAll = function() {
    return this.replace(/\s/, "");
}

String.prototype.ToNumber = function () {

    var result = 0;

    result = parseFloat(this);

    if (isNaN(result)) {
        //var convertEx
        throw new userException({ name:"Conversión inválida", description: "  '" + this + "' no puede ser convertido a nùmero. ", functionDefinition: arguments.callee, pageName: window.location.href, severity: errSevy.low });
    }

    return result;
}


// Function IsNullOrEmpty 
// Regresa verdadero cuando el objeto es nulo o una cadena vacía.
function IsNullOrEmpty(object) {
    
    if (typeof(object) === 'undefined' || object === null)
        return true;
    if (object === "")
        return true;
    return false;
}

// Function IsNullOrEmpty 
// Regresa verdadero cuando el objeto es nulo o una cadena vacía.
function IsNullOrEmptyWhite(object) {

    if (typeof (object) === 'undefined' || object === null)
        return true;
    if (object === "")
        return true;
    if (jQuery.trim(object) === "")
        return true;
    return false;
}

// Function IsNullOrEmptyOrZero 
// Regresa verdadero cuando el objeto es nulo o una cadena vacía.
function IsNullOrEmptyOrZero(object) {
    if (object === undefined)
        return true;
    if (object === "")
        return true;
    if (object === "0")
        return true;
    if (object === 0)
        return true;
    return false;
}

//- Establece el valor de una Cookie
function setCookie(c_name, value, exdays) {
    var exdate = new Date();

    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays === null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x === c_name) {
            return unescape(y);
        }
    }
}

function StringBuilder(/* str, str, str */) {
    /// <summary>Cadena mutable</summary>
    
    this._array = [];
    this._index = 0;

    this.Append = function (/* str, str, str, ... */) {
        /// <summary>Agrega un string a la cadena mutable</summary>
        /// <param type="String">Cadena que se concatena </param>

        for (var i = 0; i < arguments.length; i++) {
            this._array[this._index] = arguments[i];
            this._index++;
        }
    }

    this.Append.apply(this, arguments);

    this.toString = function () {
        /// <summary>Regresa el string que contiene la cadena mutable</summary>
        return this._array.join('');
    }

}

function serializeJSonToString(jsonOject) {
    /// <summary>Crea un string con formato json</summary>
    /// <param type="JSon">Paràmetros</param>

    var action = "";

    var appendcomma = false;

    if (typeof (jsonOject) != 'undefined' && jsonOject != null) {
        action += "{"

        $.each(jsonOject, function (key, value) {
            if (value === null) {
                action += '{1}{0}:null'.format(key, appendcomma ? ',' : '');
            }
            else if (jQuery.trim(value) === "") {
                action += '{1}{0}:\'\''.format(key, appendcomma ? "," : "");
            }
            else {
                if (isNaN(value)) {
                    action += '{2}{0}:\'{1}\''.format(key, value, appendcomma ? ',' : '');
                }
                else {
                    action += '{2}{0}:{1}'.format(key, value, appendcomma ? ',' : '');
                }
            }

            appendcomma = true;

        });

        action += "}"
    }

    return action;
}
