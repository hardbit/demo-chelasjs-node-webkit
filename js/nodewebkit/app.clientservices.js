/**
* Modulo que realiza llamadas a servicios REST
* 
*/

"user strict";

(function() {
	namespace("App.ClientServices", getCustomers);

	var http = require('http');

	function getCustomers (callback) {
		var body = '';
		var options = {
			hostname: 'query.yahooapis.com',
			port: 80,
			path: '/v1/public/yql?q=SELECT%20*%20FROM%20weather.forecast%20WHERE%20woeid%3D%22116545%22&format=json',
			method: 'GET'
		};

		var req = http.request(options, function(res) {
			console.log("statusCode: ", res.statusCode);

			res.setEncoding('utf8');
			res.on('data', function (chunk) {
				body += chunk;
			});
			res.on('end', function () {
				callback(body);
			});
		});
		req.end();

		req.on('error', function(e) {
			console.error(e);
		});
	}

}) ();
