# Demo ChelaJS

## node-webkit
Lo primero que necesitara para correr este ejemplo es descargar el runtime de node-webkit en la siguiente url [https://github.com/rogerwang/node-webkit](http://)
Tambien necesitara un editor de textos ;)

## Creacion del paquete
Dentro del directorio de tools se encuentra el archivo **buildmacnw.sh**, que creara el archivo con extension nw para que pueda ser ejecutado en el runtime de node-webkit

## Servicios y libs de terceros
Se utilizaron los siguientes servicios y libs
- Yahoo Weather
- jQuery
- namespace
- bootstrap
- fontawesome